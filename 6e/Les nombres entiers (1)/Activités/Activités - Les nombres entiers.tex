\documentclass[french,12pt]{article}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage[a4paper,landscape]{geometry}
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{accents}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{fancybox}
\usepackage{colortbl}
\definecolor{lightgray}{gray}{0.95}
\definecolor{darkgray}{gray}{0.75}
\definecolor{myblue}{RGB}{0,128,255}
\definecolor{mygreen}{RGB}{0, 128, 0}
\definecolor{myred}{RGB}{255, 0, 0}
\usepackage{eurosym}
\usepackage{frcursive}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{siunitx}
\sisetup{
	locale = FR,   % Définit le langage français
	group-minimum-digits=4,
}

\usepackage{sectsty}
\allsectionsfont{\sffamily}

\newenvironment{solution}
{\renewcommand\qedsymbol{$\blacksquare$}
	\begin{proof}[Solution]}
	{\end{proof}}
\renewcommand\qedsymbol{$\blacksquare$}

\newcommand{\ubar}[1]{\underaccent{\bar}{#1}}

\renewcommand\familydefault{\sfdefault}

\geometry{%
	a4paper, landscape,
	body={280mm,175mm},
	left=8mm, top=18mm,
	headheight=9mm, headsep=3mm}

\pagestyle{fancy}
\fancyhf{}
\lhead{Collège Victor Vasarely \\ M. Iaconelli}
\chead{Les nombres entiers \\ Fiche d'activités}
\rhead{Mathématiques - 6e \\ 2023-2024}
\cfoot{\thepage\ sur \pageref{LastPage}}
\rfoot{\texttt{www.lmdbt.fr}}
\usepackage{ccicons}\lfoot{\ccby}

\setstretch{1}

\newcounter{NumAct}
\setcounter{NumAct}{1}

\newcommand{\incrementNumAct}{\stepcounter{NumAct}}

\setlength{\parindent}{0pt}

\begin{document}
	
	\begin{Large}
		
		\begin{center}
			\setlength{\fboxsep}{2mm}% définir l'écart
			\setlength{\fboxrule}{0.5mm} % définir l'épaisseur du trait
			\fbox{\LARGE{\textbf{Les nombres entiers - Fiche d'activités}}}
		\end{center}
		
		\noindent
		\begin{minipage}{0.28\textwidth{}}
			\includegraphics[width=\textwidth]{Objectifs.pdf}
			\medskip
		\end{minipage}
		\hfill
		\begin{minipage}{0.7\textwidth{}}
			\textbf{Activité \arabic{NumAct}\incrementNumAct{} - La décomposition d'un nombre entier}
			\medskip
			
			Roxane remarque que le nombre $\num{7538}$ peut s'écrire de la façon suivante : \\ $\num{7000}+500+30+8$.
			
			D'autre part, Roxane sait que $\num{7000}=7\times\num{1000}$, que $500=5\times100$, etc.
			
			Roxane conclut donc qu'elle peut décomposer $\num{7538}$ de la manière suivante : \\
			$\num{7538}=\num{7000}+500+30+8$ \\
			$\num{7538}=7\times\num{1000}+5\times100+3\times10+8\times1$
			
			\begin{enumerate}
				\item À l'aide du raisonnement de Roxane, \textbf{décomposer} les 5 nombres ci-dessous :
				\vspace{-0.25cm}
				
				\begin{multicols}{2}
					\begin{enumerate}[label=\alph*.]
						\item $\num{4531}$
						\item $\num{12849}$
						\item $\num{5079}$
						\item $\num{907604}$ 
					\end{enumerate}
				\end{multicols}
				\vspace{-0.75cm}
				
				\begin{enumerate}[label=\alph*.]
					\setcounter{enumii}{4}
					\item $\textrm{Soixante-dix-sept-mille-huit-cent-douze}$
				\end{enumerate}
			\end{enumerate}
		\end{minipage}
		\medskip
		
		\noindent
		\begin{enumerate}
			\setcounter{enumi}{1}
			\item Son ami Pablo lui propose de faire l'inverse, c'est-à-dire d'\textbf{écrire} le nombre à partir de sa décomposition :
			\vspace{-0.25cm}
			
			\begin{multicols}{2}
				\begin{enumerate}[label=\alph*.]
					\item $(7\times\num{1000})+(5\times100)+(2\times10)+8\times1$
					\item $(1\times\num{10000})+(1\times100)+1\times1$
					\item $(3\times\num{100000})+(7\times\num{10000})+(4\times10)+9$
					\item $(5\times\num{100000000})+(4\times\num{10000})$
				\end{enumerate}
			\end{multicols}
			
			\item Théo propose la décomposition suivante : $(35\times\num{1000})+(43\times100)+9$. \\
			Roxane lui fait remarquer qu'il a dû faire une erreur.
			\vspace{-0.25cm}
			
			\begin{multicols}{2}
				\begin{enumerate}[label=\alph*.]
					\item Quel est le problème avec la décomposition de Théo ?
					\item \textbf{Écrire} ce nombre sous la forme d'un nombre entier
					\item \textbf{Écrire}, finalement, la décomposition correcte de ce nombre.
				\end{enumerate}
			\end{multicols}
		\end{enumerate}
		\clearpage
		
		\textbf{Activité \arabic{NumAct}\incrementNumAct{} - Identifier un chiffre dans un nombre}
		\medskip
		
		\begin{minipage}{0.65\textwidth}
			\begin{enumerate}[label=\alph*.]
				\item Si on range le nombre $\num{2 987 654 321}$ dans le tableau ci-contre. On peut lire facilement que :
				\vspace{-0.5cm}
				
				\begin{itemize}[label=\textbullet]
					\item le chiffre $5$ est celui des \textcursive{dizaines de milliers}.
					\vspace{-0.5cm}
					
				\end{itemize}
				\textbf{Compléter} les pointillés ci-dessous :
				\vspace{-0.5cm}
				
				\begin{itemize}[label=\textbullet]
					\onehalfspacing 
					\item le chiffre $8$ est celui des \makebox[5cm]{\dotfill} \makebox[5cm]{\dotfill}
					\item le chiffre $7$ est celui des \makebox[5cm]{\dotfill} \makebox[5cm]{\dotfill}
					\item le chiffre $9$ est celui des \makebox[5cm]{\dotfill} \makebox[5cm]{\dotfill}
				\end{itemize}
			\end{enumerate}
		\end{minipage}
		\hfill
		\begin{minipage}{0.33\textwidth}		
			\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}
				\hline
				\multicolumn{3}{|c|}{\cellcolor{lightgray}milliards} &
				\multicolumn{3}{c|}{\cellcolor{lightgray}millions} & \multicolumn{3}{c|}{\cellcolor{lightgray}milliers} & \multicolumn{3}{c|}{\cellcolor{lightgray}unités}  \\ \hline
				\cellcolor{myred!10}c & \cellcolor{myblue!10}d & \cellcolor{mygreen!10}u & \cellcolor{myred!10}c & \cellcolor{myblue!10}d & \cellcolor{mygreen!10}u & \cellcolor{myred!10}c & \cellcolor{myblue!10}d & \cellcolor{mygreen!10}u & \cellcolor{myred!10}c & \cellcolor{myblue!10}d & \cellcolor{mygreen!10}u \\
				\hline
				\textcolor{white}{$0$} & \textcolor{white}{$0$} & $2$ & $9$ & $8$ & $7$ & $6$ & $5$ & $4$ & $3$ & $2$ & $1$ \\
				\hline
			\end{tabular}
		\end{minipage}
		
		\begin{enumerate}[label=\alph*.]
			\setcounter{enumi}{1}
			\item \textbf{Recopier} (sur votre feuille et à la règle) le tableau précédent et le \textbf{compléter} avec le nombre : \num{382 105 324 897}.
			\item \textbf{Compléter} les phrases ci-dessous avec le bon chiffre :
		\end{enumerate}
		
		\begin{multicols}{2}
			\begin{itemize}[label=\textbullet]
				\onehalfspacing 
				\item le chiffre des dizaines de millions est le  \makebox[1cm]{\dotfill}
				\item le chiffre des centaines de milliards est le  \makebox[1cm]{\dotfill}
				\item le chiffre des milliers est le  \makebox[1cm]{\dotfill}
				\item le chiffre des dizaines de milliards est le  \makebox[1cm]{\dotfill}
			\end{itemize}
		\end{multicols}
		
		
		\textbf{Activité \arabic{NumAct}\incrementNumAct{} - Écrire avec des chiffres et écrire avec des lettres}
		
		Yasmine a écrit le nombre : $\num{2 987 654 321}$ en toutes lettres sur son cahier. On peut y lire :
		\bigskip
		
		\textcursive{Deux-milliards-neuf-cent-quatre-vingt-sept-millions-six-cent-cinquante-quatre-mille-trois-cent-vingt-et-un}\smallskip
		
		\medskip
		À partir du nombre écrit par Yasmine, \textbf{écrire} en toutes lettres des nombres suivant :
		\vspace{-0.25cm}
		
		\begin{multicols}{3}
			\begin{enumerate}[label=\alph*.]
				\item $\num{1096}$
				\item $\num{13184}$
				\item $\num{5893}$
				\item $\num{1219275200}$
				\item $\num{70000000}$
				\item $\num{132854780}$
			\end{enumerate}
		\end{multicols}
		\medskip
		
		\clearpage
		\textbf{Activité \arabic{NumAct}\incrementNumAct{} - Repérer des nombres sur une demi-droite graduée}
		\medskip
		
		\begin{minipage}{0.48\textwidth}
			\begin{enumerate}
				\item \textbf{Recopier} sur le cahier (ou sur feuille) et \textbf{compléter} les axes gradués ci-dessous :
				
				\begin{enumerate}[label=\alph*.]
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {0,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/$0$,1/$100$}
						\draw (\x,-0.2) node[below] {\label};
					\end{tikzpicture}
					\bigskip
					
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {0,2,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/$0$,10/$10$}
						\draw (\x,-0.2) node[below] {\label};
					\end{tikzpicture}
					\bigskip
					
					
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {2,4,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/\textcolor{white}{0},6/\num{900},10/\num{1000}}
						\draw (\x,-0.2) node[below] {\label};
					\end{tikzpicture}
					\bigskip
					
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {1,2,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/\textcolor{white}{0},6/\num{12000},10/\num{13000}}
						\draw (\x,-0.2) node[below] {\label};
					\end{tikzpicture}
					\bigskip
					
				\end{enumerate}
			\end{enumerate}
		\end{minipage}
		\hfill\vline\hfill
		\begin{minipage}{0.48\textwidth}
			\begin{enumerate}
				\setcounter{enumi}{1}
				\item Pour chaque axe gradué ci-dessous, \textbf{indiquer} les abscisses des points marqués :
				\begin{enumerate}[label=\alph*]
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {0,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/\textcolor{white}{$0$},1/$0$,7/$60$}
						\draw (\x,-0.2) node[below] {\label};
						
						% Points
						\foreach \x/\label in {3/$A$,6/$B$,9/$C$}
						\draw (\x,0.2) node[above] {\label};
					\end{tikzpicture}
					\bigskip
					
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {0,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/\textcolor{white}{$0$},1/\num{9800},3/\num{9900}}
						\draw (\x,-0.2) node[below] {\label};
						
						% Points
						\foreach \x/\label in {2/$D$,6/$E$,10/$F$}
						\draw (\x,0.2) node[above] {\label};
					\end{tikzpicture}
					\bigskip
					
					\item
					\begin{tikzpicture}
						% Axe horizontal
						\draw[->, line width=1pt] (0,0) -- (10.5,0);
						
						% Graduations
						\foreach \x in {0,...,10}
						\draw[line width=1pt] (\x,0.2) -- (\x,-0.2) ;
						
						% Nombres
						\foreach \x/\label in {0/\textcolor{white}{$0$},1/\num{1000},3/\num{1500}}
						\draw (\x,-0.2) node[below] {\label};
						
						% Points
						\foreach \x/\label in {3/$G$,6/$H$,9/$I$}
						\draw (\x,0.2) node[above] {\label};
					\end{tikzpicture}
					\bigskip
					
				\end{enumerate}
			\end{enumerate}
		\end{minipage}~
		
		\bigskip
		\begin{minipage}{0.48\textwidth}
			\textbf{Activité \arabic{NumAct}\incrementNumAct{} - Comparer des entiers}
			
			\begin{enumerate}
				\item \textbf{Recopier} et \textbf{compléter} avec les signes : $<$, $>$ ou~$=$.\vspace{-0.25cm}
				
				\begin{multicols}{3}
					\begin{enumerate}[label=\alph*.]
						\item \num{25}~\ldots~\num{14}
						\item \num{0}~\ldots~\num{43}
						\item $0765$~\ldots~\num{765}
						\item \num{547}~\ldots~\num{745}
						\item \num{997}~\ldots~\num{1001}
						\item \num{9909}~\ldots~\num{9099}
					\end{enumerate}
				\end{multicols}
				\medskip
				
				\item \textbf{Classer} les nombres suivants dans l'ordre croissant :
				\begin{center}
				\num{7659} - \num{7569} - \num{7666} - \num{7965} - \num{7999} - \num{7596}
				\end{center}
			\end{enumerate}
		\end{minipage}
		\hfill\vline\hfill
		\begin{minipage}{0.48\textwidth}
			\textbf{Activité \arabic{NumAct}\incrementNumAct{} - Construire une frise chronologique}
			\begin{enumerate}[label=\alph*.]
				\item \textbf{Construire} une frise chronologique d'origine \num{1640}, en prenant $1~\textrm{cm}$ pour $10~\textrm{ans}$.
				\item \textbf{Placer} ces 4 mathématiciens sur cette frise en fonction de leur date de naissance.
			\end{enumerate}
			\begin{center}
				\begin{large}
					\begin{tabular}{|l|c|l|}
						\hline
						\cellcolor{lightgray}Prénom Nom       & \cellcolor{lightgray}Année de naissance & \cellcolor{lightgray}Nationalité       \\
						\hline
						Isaac Newton     & $1642$                & Anglais         \\
						Srinivasa Ramanujan & $1887$              & Indien            \\
						Mary Cartwright  & $1900$                & Britannique       \\
						Sophie Germain   & $1776$                & Française         \\
						\hline
					\end{tabular}
				\end{large}
			\end{center}
		\end{minipage}
	\end{Large}
\end{document}
