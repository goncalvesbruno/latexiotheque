<div style="display: flex;">
  <h1 style="display: none;">La TeXiothèque de lmdbt.fr</h1>
  <h1 style="width: 89%;">La TeXiothèque de lmdbt.fr</h1>
  <h1 style="width: 20%;"><img src="https://lmdbt.fr/Logo250px.png"><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/by.svg"></h1></div>

<div style="display: flex; justify-content: space-between; text-align:center;">
<button>[Table des matières par niveau](#TableMat)</button> <button>[Présentation](#Intro)</button>   <button>[Fiches d'activités, leçon et d'exercice](#Fiches)</button> <button>[Les Pixel Arts](#PixelArt)</button> <button>[Crédits](#cred)</button> <button>[Contact](#contact)</button>
</div>

<p id="TableMat"></p>
## Table des matières
 - [Présentation du dépôt](#Intro)
 - [Les fiches d'activités, leçon et d'exercice](#Fiches)
 	- [Niveau 6e](#6e)
 		- [Les nombres entiers (1)](#nbent1)
    	- [Calculer avec les nombres entiers (2)](#nbent2)
		- [Éléments de géométrie](#ElGeo)
		- [Calculer avec les nombres décimaux](#CalcDec)
        - [Les fractions](#Fractions)
        - [Les angles](#Angles)
 	- [Niveau 5e](#5e)
 		- [Les priorités opératoires](#PriOp)
    	- [Écriture fractionnaire](#EcrFrac)
		- [Le triangle et ses angles](#TriAng)
    	- [Les droites remarquables du triangle](#DrTri)
 	- [Niveau 4e (à venir...)](#4e)
 	- [Niveau 3e (à venir...)](#3e)
 - [Les Pixel-Arts](#PixelArt)

<p id="Intro"></p>
## Présentation du dépôt

Salut et bienvenue sur mon espace dédié à l'enseignement des mathématiques au collège !

J'ai conçu cet espace pour partager librement (en CC-BY) mes ressources pédagogiques. 

### Structure des Ressources

Mes ressources sont structurées par niveau et vous y trouverez :

- **Fiches d'activités :** Des fichiers PDF et .tex contenant des activités ludiques et éducatives. Mise en page de façon a être imprimée deux par page puis découpées et collées dans le cahier.
- **Fiches leçon :** PDF et .tex avec les leçons claires et concises, adaptées aux différents niveaux du collège. Mises en page de façon à être perforrée.
- **Fiches d'exercice :** Des exercices en PDF et .tex pour renforcer la compréhension et la maîtrise des concepts. Mises en page de façon à être perforrée.
- **Illustrations :** Personnages et dialogues en PDF et ODP pour rendre l'apprentissage plus engageant et interactif.

![Exemple d'illustration](https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/FilleFraction.jpg)

### Enchaînement des Supports

Voici comment je structure mes séquences :

1. **Introduction avec une fiche d'activité :** Pour éveiller l'intérêt et engager les élèves.
2. **Intégration de la notion dans la leçon :** Utilisation des fiches leçon pour enseigner les concepts clés.
3. **Entraînement et perfectionnement avec les fiches d'exercice :** Application des concepts à travers des exercices pratiques.
4. **Utilisation d'illustrations :** Intégration de personnages et dialogues pour rendre la séquence vivante et mémorable.

![Schéma de la séquence](https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/DeroulSequ.png)

Je vous invite à explorer et à utiliser ces ressources et à me communiquer en retour toutes modifications qui vous semble pertinente.

Et si vous avez des questions ou des commentaires, n'hésite pas à me contacter également.

Bonne exploration !

<p id="Fiches"></p>
## Les fiches d'activités, leçon et d'exercice

<p id="6e"></p>
### Niveau 6e
<p id="nbent1"></p>
#### Les nombres entiers (1)

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/MiniatureFAct.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20nombres%20entiers%20(1)/Activit%C3%A9s/Activit%C3%A9s_-_Les_nombres_entiers.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/MiniatureFLec.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20nombres%20entiers%20(1)/Le%C3%A7on/Le%C3%A7on_-_Les_nombres_entiers.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/MiniatureFEx.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20nombres%20entiers%20(1)/Exercices/Exercices_-_Les_nombres_entiers.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/MiniatureFAct.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20nombres%20entiers%20(1)/Activit%C3%A9s/Activit%C3%A9s%20-%20Les%20nombres%20entiers.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/MiniatureFLec.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20nombres%20entiers%20(1)/Le%C3%A7on/Le%C3%A7on_-_Les_nombres_entiers.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/MiniatureFEx.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20nombres%20entiers%20(1)/Exercices/Exercices_-_Les_nombres_entiers.tex)
    </div>
</div>

<p id="nbent2"></p>
#### Calculer avec les nombres entiers (2)

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActCalcNbEnt6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_entier/Activites/Activit%C3%A9s%20-%20Calculer_nombres_entiers.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecCalcNbEnt6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_entier/Lecon/Le%C3%A7on%20-%20Calculer%20avec%20les%20nombres%20entiers.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeCalcNbEnt6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_entier/Exercices/Exercices%20-%20Calculer%20avec%20des%20nombres%20entiers.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActCalcNbEnt6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_entier/Activites/Activit%C3%A9s%20-%20Calculer_nombres_entiers.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecCalcNbEnt6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_entier/Lecon/Le%C3%A7on%20-%20Calculer%20avec%20les%20nombres%20entiers.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeCalcNbEnt6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_entier/Exercices/Exercices%20-%20Calculer%20avec%20des%20nombres%20entiers.tex)
    </div>
</div>

<p id="ElGeo"></p>
#### Éléments de géométrie

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActElGeo6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Elements_de_geometrie/Activites/Activites_Elements_de_geometrie.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecElGeo6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Elements_de_geometrie/Lecon/Lecon_elments_de_geometrie.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExElGeo6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Elements_de_geometrie/Exercices/Exercices_ElementDeGeometrie.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActElGeo6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Elements_de_geometrie/Activites/Activites_Elements_de_geometrie.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecElGeo6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Elements_de_geometrie/Lecon/Lecon_elments_de_geometrie.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExElGeo6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Elements_de_geometrie/Exercices/Exercices_ElementDeGeometrie.tex)
    </div>
</div>

<p id="CalcDec"></p>
#### Calculer avec des nombres décimaux

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActCalcNbDec6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_d%C3%A9cimaux/activites/Activit%C3%A9s%20-%20Calculer_nombres_d%C3%A9cimaux.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecCalcNbDec6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_d%C3%A9cimaux/Lecon/Le%C3%A7on%20-%20Calculer%20avec%20les%20nombres%20d%C3%A9cimaux.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeCalcNbDec6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_d%C3%A9cimaux/Exercices/Exercices%20-%20Calculer%20avec%20des%20nombres%20d%C3%A9cimaux.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActCalcNbDec6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_d%C3%A9cimaux/activites/Activit%C3%A9s%20-%20Calculer_nombres_d%C3%A9cimaux.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecCalcNbDec6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_d%C3%A9cimaux/Lecon/Le%C3%A7on%20-%20Calculer%20avec%20les%20nombres%20d%C3%A9cimaux.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeCalcNbDec6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Calculer_nombre_d%C3%A9cimaux/Exercices/Exercices%20-%20Calculer%20avec%20des%20nombres%20d%C3%A9cimaux.tex)
    </div>
</div>

<p id="Fractions"></p>
#### Les fractions

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActFrac6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les_Fractions/activit%C3%A9s/Activites_Fractions.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecFrac6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les_Fractions/le%C3%A7on/Lecon_Fractions.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeFrac6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les_Fractions/exercices/Exercices%20-%20Les%20Fractions.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActFrac6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les_Fractions/activit%C3%A9s/Activites_Fractions.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecFrac6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les_Fractions/le%C3%A7on/Lecon_Fractions.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeFrac6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les_Fractions/exercices/Exercices%20-%20Les%20Fractions.tex)
    </div>
</div>

<p id="Angles"></p>
#### Les angles

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActAngle6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20angles/Activit%C3%A9s/Activites_Les_Angles.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecAngle6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20angles/Le%C3%A7on/Lecon_Les_Angles.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeAngle6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20angles/Exercices/Exercices%20-%20Les%20angles.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActAngle6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20angles/Activit%C3%A9s/Activites_Les_Angles.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecAngle6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20angles/Le%C3%A7on/Lecon_Les_Angles.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeAngle6e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/6e/Les%20angles/Exercices/Exercices%20-%20Les%20angles.tex)
    </div>
</div>

<p id="5e"></p>
### Niveau 5e
<p id="PriOp"></p>
#### Les priorités opératoires

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActPriOp5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Priorit%C3%A9s%20op%C3%A9ratoires/Activit%C3%A9s/Activit%C3%A9s_-_PriOp.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecPriOp5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Priorit%C3%A9s%20op%C3%A9ratoires/Le%C3%A7on/Le%C3%A7on_-_PriOp.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExPriOp5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Priorit%C3%A9s%20op%C3%A9ratoires/Exercices/Exercices_-_PriOp.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActPriOp5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Priorit%C3%A9s%20op%C3%A9ratoires/Activit%C3%A9s/Activit%C3%A9s_-_PriOp.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecPriOp5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Priorit%C3%A9s%20op%C3%A9ratoires/Le%C3%A7on/Le%C3%A7on_-_PriOp.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExPriOp5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Priorit%C3%A9s%20op%C3%A9ratoires/Exercices/Exercices_-_PriOp.tex)
    </div>
</div>

<p id="EcrFrac"></p>
#### Écriture fractionnaire

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActFrac5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Ecriture_fractionnaire/activites/Activites_Fractions.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecFrac5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Ecriture_fractionnaire/lecon/Lecon_Fractions.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExFrac5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Ecriture_fractionnaire/exercices/Exercices_Fractions.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActFrac5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Ecriture_fractionnaire/activites/Activites_Fractions.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecFrac5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Ecriture_fractionnaire/lecon/Lecon_Fractions.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExFrac5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Ecriture_fractionnaire/exercices/Exercices_Fractions.tex)
    </div>
</div>

<p id="TriAng"></p>
#### Le triangle et ses angles

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActTriAng5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Le_triangle_et_ses_angles/Activites/Activit%C3%A9s%20-%20Triangle.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecTriAng5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Le_triangle_et_ses_angles/Lecon/Le%C3%A7on%20-%20Triangle.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExTriAng5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Le_triangle_et_ses_angles/Exercices/Exercices%20-%20Triangles.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActTriAng5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Le_triangle_et_ses_angles/Activites/Activit%C3%A9s%20-%20Triangle.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecTriAng5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Le_triangle_et_ses_angles/Lecon/Le%C3%A7on%20-%20Triangle.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExTriAng5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Le_triangle_et_ses_angles/Exercices/Exercices%20-%20Triangles.tex)
    </div>
</div>

<p id="DrTri"></p>
#### Les droites remarquables du triangle

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActDrTri5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/droites_remarquables_triangles/Activites/Activit%C3%A9s%20-%20Triangle%20et%20droites%20remarquables.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecDrTri5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/droites_remarquables_triangles/Lecon/Le%C3%A7on%20-%20Droites%20remarquables%20du%20triangle.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExDrTri5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/droites_remarquables_triangles/Exercices/Exercices%20-%20Droites%20remarquables%20du%20triangle.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActDrTri5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/droites_remarquables_triangles/Activites/Activit%C3%A9s%20-%20Triangle%20et%20droites%20remarquables.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecDrTri5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/droites_remarquables_triangles/Lecon/Le%C3%A7on%20-%20Droites%20remarquables%20du%20triangle.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExDrTri5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/droites_remarquables_triangles/Exercices/Exercices%20-%20Droites%20remarquables%20du%20triangle.tex)
    </div>
</div>

<p id="NbRel"></p>
#### Les nombres relatifs

**Au format PDF <strong class="fa-solid fa-file-pdf"></strong>:**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActNbRel5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Les%20nombres%20relatifs/Activites/Activit%C3%A9s%20-%20Nombres%20relatifs.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecNbRel5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Les%20nombres%20relatifs/Lecon/Le%C3%A7on%20-%20Additions%20soustractions.pdf)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeNbRel5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Les%20nombres%20relatifs/Exercices/Exercices%20-%20Additions%20soustractions.pdf)
    </div>
</div>

**Au format $\LaTeX$ :**
[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-pen-ruler"></b> Fiche d'activités</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ActNbRel5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Les%20nombres%20relatifs/Activites/Activit%C3%A9s%20-%20Nombres%20relatifs.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-leanpub"></b> Fiche leçon </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/LecNbRel5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Les%20nombres%20relatifs/Lecon/Le%C3%A7on%20-%20Additions%20soustractions.tex)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-dumbbell"></b> Fiche d'exercices</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ExeNbRel5e.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/blob/main/5e/Les%20nombres%20relatifs/Exercices/Exercices%20-%20Additions%20soustractions.tex)
    </div>
</div>

<p id="PixelArt"></p>
## Les Pixel-Arts

### À propos

Depuis le début d'année 2024, je réalise des pixel-arts pédagogiques. Présents dans les fiches d'exercices mais également pour animer des séances de remédiation en "renfort-maths".

L'ensemble de ces Pixel-Arts sont présents sur [cet espace du dépot](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt) et concernent, pour l'instant, principalement les niveaux de 6e et 5e (mais peuvent aussi enrichir des séances de remédiation pour des niveaux supérieurs).

L'ensemble de ces fiches sont en licence [<b class="fa-brands fa-creative-commons"></b><b class="fa-brands fa-creative-commons-by"></b> 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr) et réalisé à l'aide de l'application [Pixel-It <b class="fa-brands fa-space-awesome"></b>](https://lmdbt.forge.apps.education.fr/pixel-it/).

### Table des matières des pixel-arts

- [La proportionnalité <b class="fa-solid fa-percent"></b>](#prop_pa)
- [Les fractions <b class="fa-solid fa-chart-pie"></b>](#frac_pa)
- [Opérations sur les nombres décimaux - 6e <b class="fa-solid fa-calculator"></b>](#operation)
- [Les résolutions de problèmes - 6e <b class="fa-solid fa-brain"></b>](#resol_pa)
- [Les périmètres et les aires <b class="fa-solid fa-chart-area"></b>](#periaire_pa)
- [Les angles <b class="fa-solid fa-compass-drafting"></b>](#ang_pa)
- [Et même en histoire <b class="fa-solid fa-clock-rotate-left"></b>](#hist_pa)

### Liens vers les documents par thème

<p id="prop_pa"></p>
#### La proportionnalité

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-rocket"></b> Vitesse, distance et temps (5e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/astronaute.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Astronaute)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-earlybirds"></b> Problèmes de proportionnalité (6e) </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/cardinal.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Cardinal)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-paw"></b> Problèmes de proportionnalité (5e)</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/koala.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Koala)
    </div>
</div>

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-bone"></b> Conversions de durée (5e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/happydog.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/HappyDog)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-hippo"></b> Les pourcentages (5e) </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/phaco.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Phacoch%C3%A8re)
    </div>
    [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-medal"></b> Calcul de pourcentage - Jeux Olympiques (5e/4e)</p><div style="display: flex;"><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/olympiques2.png"></div>](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/FlammeOlympique)
    </div>
</div>

<p id="frac_pa"></p>
#### Les fractions

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-paw"></b> Les fractions (6e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/stitch.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Stitch)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-firefox"></b> Les fractions (6e) </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/renard.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Renard)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-wrench"></b> Résolution de problème avec des fractions (6e) </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/mariofrac.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Mario)
    </div>
</div>

<p id="operation"></p>
#### Opérations sur les nombres décimaux - 6e

[<div style="display: flex; justify-content: space-between;">
    <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-paw"></b> Opérations sur les nombres décimaux (6e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/lionhead.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/LionHead)
    </div>
  <div></div>
  <div></div>
</div>

<p id="resol_pa"></p>
#### Les résolutions de problèmes - 6e

##### Avec des nombres décimaux

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-paw"></b> Résolution de problèmes (6e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/ours.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Ours)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-solid fa-wand-sparkles"></b> Résolution de problèmes (6e) </p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/hp.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/HP)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"><b class="fa-brands fa-reddit-alien"></b> Résolution de problèmes (6e)</p><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/yd.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/YD)
    </div>
</div>

<p id="periaire_pa"></p>
#### Les périmètres et les aires

<div style="display: flex; justify-content: space-between;">
    [<div style="width: 45%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-cat"></b> Périmètres et aires (6e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/chatchibi.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Chatchibi)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-paw"></b> Périmètres et aires (5e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/tigre.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Tigre)
    </div>
  <div></div>
</div>

<p id="ang_pa"></p>
#### Les angles

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-wrench"></b> Les angles (6e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/marioangles.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/MarioAngles)
    </div>
  [<div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-wrench"></b> Les angles dans le triangles (5e)</p> <img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/CitrouilleAngele.png">](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Citrouille-Angele)
    </div>
  <div></div>
</div>

<p id="hist_pa"></p>
#### Et même en histoire

[<div style="display: flex; justify-content: space-between;">
  <div style="width: 30%; solid #000;"><p style="text-align : center;"> <b class="fa-solid fa-crown"></b> Charlemagne (5e)</p> <div style="display: flex;"><img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/charlemagne1.png">$~$<img src="https://forge.apps.education.fr/lmdbt/latexiotheque/-/raw/main/IMG/charlemagne2.png"></div>](https://forge.apps.education.fr/lmdbt/latexiotheque/-/tree/main/PixelArt/Charlemagne)
    </div>
  <div></div>
  <div></div>
</div>


<p id="cred"></p>
## Crédits et Remerciements

Je tiens à exprimer ma gratitude envers tous ceux qui ont rendu ce travail possible et qui continuent à contribuer à son développement et son amélioration.

### Sésamath

Un grand merci à [Sésamath](https://www.sesamath.net/) dont de nombreuses activités sont issues ou inspirées.
Leur engagement envers une éducation de qualité et accessible a été une source d'inspiration majeure pour moi.

### Génération d'images par IA par Dall.e 3

Les dessins me servant de support pour construire les dialogues sont issus de cette IA.

### Collègues de La Forge

Merci aux collègues à l'initiative de La Forge. Votre passion et votre dévouement pour l'éducation ont été un moteur puissant derrière ce projet.

### Relecteurs et Participants

Je souhaite également remercier tous ceux, collègues et autres, qui ont participé et qui vont participer à la relecture de ce travail.
Votre expertise et vos retours constructifs sont inestimables pour assurer la qualité et l'exactitude des ressources partagées.

### À Tous

Enfin, merci à tous ceux qui visitent, utilisent et partagent ces ressources. Ensemble, nous contribuons à créer un monde où l'éducation est accessible et engageante pour tous.

<p id="contact"></p>
## Contact :
N'hésitez pas à me contacter si vous avez des questions, des suggestions ou si vous souhaitez contribuer à ce projet :

<i class="fa-solid fa-envelope-circle-check"></i> : cyril<i class="fa-solid fa-circle"></i>iaconelli<i class="fa-solid fa-at"></i>ac-rennes.fr